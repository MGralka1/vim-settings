" Load plugins configuration
source ~/.vim/config/plugins.vim

colorscheme solarized
syntax on
set background=dark

set number

set exrc

nnoremap <C-l> <C-w><C-l>
nnoremap <C-h> <C-w><C-h>
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>

execute "set <M-h>=\eh"
execute "set <M-l>=\el"
nnoremap <M-l> :bn<CR>
nnoremap <M-h> :bp<CR>

nnoremap - :NERDTreeToggle<CR>

nnoremap <S-Tab> :A<CR>

