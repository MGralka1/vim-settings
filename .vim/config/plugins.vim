"Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
Plug 'tpope/vim-sensible'
Plug 'mikewest/vimroom'

Plug 'valloric/youcompleteme'

" Colorschemes
Plug 'junegunn/seoul256.vim'
Plug 'altercation/vim-colors-solarized'

Plug 'scrooloose/nerdtree'
Plug 'bling/vim-airline'

"C++ switch between hpp/cpp
Plug 'vim-scripts/a.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

let g:ycm_server_python_interpreter='python3'

"bling/vim-airline
let g:airline#extensions#tabline#enabled = 1
