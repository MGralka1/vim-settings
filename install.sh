#!/bin/bash

cd ${HOME}

ln -s ${HOME}/Dokumenty/vim-settings/.vimrc .vimrc
ln -s ${HOME}/Dokumenty/vim-settings/.vim .vim

apt install curl && curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
